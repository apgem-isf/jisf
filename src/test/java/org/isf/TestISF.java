package org.isf;

import java.io.IOException;
import java.net.URISyntaxException;

import org.isf.formal.*;

public class TestISF {

	public static void main(String[] args) throws IOException,
			URISyntaxException {
		FormalLevel fm = new FormalLevel("http://isf.org/datatest");

		Project test = fm.createProject("test");
		test.setLabel("test");

		Subject testSubject = test.createSubject("testSubject");
		testSubject.setLabel("A80001");
		// some demographics
		Demographics demCollection = testSubject
				.createDemographics("testSubject");
		Dataset demDataset = demCollection.createDataset();
		// item for gender
		DataItem gender = demDataset.createDataItem("gender");
		gender.setValue("female");
		// item for year of birth
		DataItem yob = demDataset.createDataItem("yob");
		yob.setValue(1980);

		// An experiment for biochemistry collection
		DataCollection biochCollection = testSubject
				.createDatacollection("Bioch0001");
		// the main data
		Dataset biochData = biochCollection.createDataset();
		// it has two subdatasets, one for bloodwork another for CSF
		Dataset blData = biochData.createDataset();
		blData.setTransformedFrom("bioch:blData");
		Dataset csfData = biochData.createDataset();
		csfData.setTransformedFrom("bioch:csfData");
		// APOE in blData
		blData.createDataItem("APOE").setValue("E3/E3");
		// abeta42 in csf
		csfData.createDataItem("abeta42").setValue(70.2);

		// MRSession
		Session session = testSubject.createSession("testMRI001");

		// a scan in the session
		Scan scan = session.addScan("1",
				"1.3.6.1.4.1.20468.2.26.0.1.4993.44676.2");
		scan.wasAttributedTo(testSubject);
		// add a dicom tag
		DataItem item = scan.createDataItem("series_description");
		item.setValue("loc_");
		item.addDicomClass("dicom_0008103E");

		// serialize the turtle RDF file
		fm.serialize("test.ttl");
	}

}
