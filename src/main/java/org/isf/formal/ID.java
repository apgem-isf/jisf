package org.isf.formal;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.Resource;

public class ID extends AbstractEntity {

	protected String className;

	public ID(String value, FormalLevel fm, AbstractEntity entity) {
		super(fm);
		this.className = "SIO_000731";
		init(value, entity);
	}

	public ID(String value, FormalLevel fm, Model model, AbstractEntity entity) {
		super(fm, model);
		this.className = "SIO_000731";
		init(value, entity);
	}

	protected void init(String value, AbstractEntity entity) {
		this.entityType = "ID";

		this.thisResource = this.model.createResource(getURIUID(),
				fm.getSIOClass(className));
		// 'is identifier for' the entity
		Property p = fm.getSIOisIdentifierFor();
		Resource r = entity.getResource();
		this.thisResource.addProperty(p, r);
		// and has the value
		this.thisResource.addLiteral(fm.getSIOHasValue(), value);

	}

}
