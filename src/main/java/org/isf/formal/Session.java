package org.isf.formal;

import org.apache.jena.ontology.OntClass;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.vocabulary.RDF;

public class Session extends DataCollection {

	private Dataset sessionData;

	public Session(String id, FormalLevel fm, Subject subject) {
		super(id, fm, subject);

	}

	public Session(String id, FormalLevel fm, Model model, Subject subject) {
		super(id, fm, model, subject);

	}

	@Override
	protected void init(String id, Subject subject) {
		super.init(id, subject);
		// add NIDM type
		OntClass ses = fm.getNIDMClass("Session");
		this.thisResource.addProperty(RDF.type, ses);

		// PROV associations
		wasAssociatedWith(subject);

		// init main dataset
		this.sessionData = createDataset();
	}

	public Scan addScan(String id, String uid) {
		Scan scan = new Scan(id, fm, model, this);
		scan.setUID(uid);
		scan.addDicomTagValue("dicom_0040A124", uid);
		return scan;
	}

	public void wasAssociatedWith(AbstractEntity resource) {
		this.thisResource.addProperty(fm.getPROVAssociatedWith(),
				resource.getResource());
	}

	public void wasAssociatedWith(Resource resource) {
		this.thisResource.addProperty(fm.getPROVAssociatedWith(), resource);
	}

	public void wasAssociatedWith(String resource) {
		this.thisResource.addProperty(fm.getPROVAssociatedWith(), resource);
	}

	public Dataset getSessionData() {
		return sessionData;
	}

}
