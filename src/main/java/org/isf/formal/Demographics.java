package org.isf.formal;

import java.util.ArrayList;

import org.apache.jena.ontology.OntClass;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.vocabulary.RDF;

public class Demographics extends DataCollection {
	public Demographics(String id, FormalLevel fm, Subject subject) {
		super(id, fm, subject);

	}

	public Demographics(String id, FormalLevel fm, Model model, Subject subject) {
		super(id, fm, model, subject);

	}

	@Override
	protected void init(String id, Subject subject) {
		this.entityType = "demographics";
		this.datasets = new ArrayList<Dataset>();
		this.thisResource = this.model.createResource(getURIFromID(id) + "_"
				+ this.entityType, fm.getSIOClass(DATA_COLLECTION_SIO));

		// add NIDM demographics type
		OntClass acqu = fm.getNIDMClass("DemographicsDataAcquisition");
		this.thisResource.addProperty(RDF.type, acqu);

		// 'has participant' the subject
		this.thisResource.addProperty(fm.getSIOhasParticipant(),
				subject.getResource());

		this.setID(id);
	}
}
