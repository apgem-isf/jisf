package org.isf.formal;

import java.util.ArrayList;
import java.util.List;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Resource;

public class DataCollection extends AbstractEntity {

	protected static final String DATA_COLLECTION_SIO = "SIO_001052";

	protected Subject subject;
	protected Project owner;
	protected List<Dataset> datasets;

	public DataCollection(String id, FormalLevel fm, Subject subject) {
		super(fm);
		this.model = ModelFactory.createDefaultModel();
		init(id, subject);
	}

	public DataCollection(String id, FormalLevel fm, Model model,
			Subject subject) {
		super(fm, model);
		init(id, subject);
	}

	protected void init(String id, Subject subject) {
		this.subject = subject;
		this.datasets = new ArrayList<Dataset>();
		this.entityType = "experiment";
		this.thisResource = this.model.createResource(getURIFromID(id),
				fm.getSIOClass(DATA_COLLECTION_SIO));

		// 'has participant' the subject
		this.thisResource.addProperty(fm.getSIOhasParticipant(),
				subject.getResource());

		this.setID(id);
	}

	public Dataset createDataset() {
		Dataset set = new Dataset(fm, this.model, this);
		// 'has output' the main dataset
		this.thisResource.addProperty(fm.getSIOhasOutput(), set.getResource());
		this.datasets.add(set);
		return set;
	}

	public void hasParticipant(AbstractEntity resource) {
		this.thisResource.addProperty(fm.getSIOhasParticipant(),
				resource.getResource());
	}

	public void hasParticipant(Resource resource) {
		this.thisResource.addProperty(fm.getSIOhasParticipant(), resource);
	}

	public void hasParticipant(String resource) {
		this.thisResource.addProperty(fm.getSIOhasParticipant(), resource);
	}

	public void isPartOf(AbstractEntity resource) {
		this.thisResource.addProperty(fm.getSIOisPartOf(),
				resource.getResource());
	}

	public void isPartOf(Resource resource) {
		this.thisResource.addProperty(fm.getSIOisPartOf(), resource);
	}

	public void isPartOf(String resource) {
		this.thisResource.addProperty(fm.getSIOisPartOf(), resource);
	}

	public void hasPart(AbstractEntity resource) {
		this.thisResource.addProperty(fm.getSIOhasPart(),
				resource.getResource());
	}

	public void hasPart(Resource resource) {
		this.thisResource.addProperty(fm.getSIOhasPart(), resource);
	}

	public void hasPart(String resource) {
		this.thisResource.addProperty(fm.getSIOhasPart(), resource);
	}

	public void setOwner(Project owner) {
		this.isPartOf(owner);
		this.owner = owner;
	}

	public Project getOwner() {
		return owner;
	}

	public Subject getSubject() {
		return subject;
	}

	public List<Dataset> getDatsets() {
		return datasets;
	}

}
