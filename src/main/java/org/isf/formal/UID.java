package org.isf.formal;

import org.apache.jena.rdf.model.Model;

public class UID extends ID {

	public UID( String value, FormalLevel fm, AbstractEntity entity) {
		super(value, fm, entity);

	}

	public UID( String value, FormalLevel fm, Model model,
			AbstractEntity entity) {
		super(value, fm, model, entity);

	}

	@Override
	protected void init( String value, AbstractEntity entity) {

		this.className = "SIO_000675";
		super.init( value, entity);

	}

}
