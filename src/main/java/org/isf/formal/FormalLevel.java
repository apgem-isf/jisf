package org.isf.formal;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import org.apache.jena.ontology.OntClass;
import org.apache.jena.ontology.OntModel;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.vocabulary.RDF;
import org.apache.jena.vocabulary.RDFS;
import org.apache.jena.vocabulary.XSD;

public class FormalLevel {

	public static final String SIO_NS = "http://semanticscience.org/resource/";
	public static final String NIDM_NS = "http://purl.org/nidash/nidm#";
	public static final String PROV_NS = "http://www.w3.org/ns/prov#";
	public static final String DICOM_NS = "http://purl.org/nidash/dicom#";

	private Map<String, String> prefixes;

	private URL dataPrefix;

	private OntModel ontologyModel;
	private Model defaultModel = ModelFactory.createDefaultModel();

	public FormalLevel() throws IOException {
		InputStream sio = new FileInputStream(new File(
				"resources/ontologies/sio.owl"));

		InputStream nidm = new FileInputStream(
				new File(
						"resources/ontologies/nidm/nidm-experiment/terms/nidm-experiment.owl"));

		InputStream dicom = new FileInputStream(new File(
				"resources/ontologies/dicom_ontology_nlx.owl"));

		ontologyModel = ModelFactory.createOntologyModel();

		ontologyModel.read(nidm, null, "TTL");

		ontologyModel.read(sio, null);
		ontologyModel.read(dicom, null, "TTL");

		initPrefixMap();

	}

	public FormalLevel(URL dataUri) throws IOException {
		this();
		this.dataPrefix = dataUri;
	}

	public FormalLevel(String dataUri) throws IOException, URISyntaxException {
		this();
		this.dataPrefix = new URI(dataUri).toURL();
	}

	private void initPrefixMap() {
		prefixes = new HashMap<String, String>();
		prefixes.put("rdf", RDF.getURI());
		prefixes.put("rdfs", RDFS.getURI());
		prefixes.put("xsd", XSD.getURI());
		prefixes.put("sio", SIO_NS);
		prefixes.put("nidm", NIDM_NS);
		prefixes.put("prov", PROV_NS);
		prefixes.put("dicom", DICOM_NS);

		this.defaultModel.setNsPrefixes(prefixes);
	}

	public Map<String, String> getPrefixes() {
		return this.prefixes;
	}

	public void addPrefix(String prefix, String url) {
		prefixes.put(prefix, url);
	}

	public Model getDefaultModel() {
		return this.defaultModel;
	}

	public void setDefaultModel(Model m) {
		this.defaultModel = m;
	}

	public void setDataPrefix(String dataURIPrefix) throws URISyntaxException,
			MalformedURLException {
		this.dataPrefix = new URI(dataURIPrefix).toURL();
	}

	public void setDataPrefix(URL dataURIPrefix) {
		this.dataPrefix = dataURIPrefix;
	}

	public String getDataPrefixString() {
		return this.dataPrefix.toString();
	}

	public URL getDataPrefix() {
		return this.dataPrefix;
	}

	public OntClass getSIOClass(String localName) {
		return ontologyModel.getOntClass(SIO_NS + localName);
	}

	public OntClass getNIDMClass(String localName) {
		String c = NIDM_NS + localName;
		return ontologyModel.getOntClass(c);
	}

	public OntClass getPROVClass(String localName) {
		return ontologyModel.getOntClass(PROV_NS + localName);
	}

	public Property getSIOhasParticipant() {
		return ontologyModel.getProperty(SIO_NS + "SIO_000132");
	}

	public Property getSIOparticipatesIn() {
		return ontologyModel.getProperty(SIO_NS + "SIO_000062");
	}

	public Property getSIOisAbout() {
		return ontologyModel.getProperty(SIO_NS + "SIO_000332");
	}

	public Property getSIOisSubjectOf() {
		return ontologyModel.getProperty(SIO_NS + "SIO_000629");
	}

	public Property getSIOhasOutput() {
		return ontologyModel.getProperty(SIO_NS + "SIO_000312");
	}

	public Property getSIOisOutputOf() {
		return ontologyModel.getProperty(SIO_NS + "SIO_000311");
	}

	public Property getSIOhasPart() {
		return ontologyModel.getProperty(SIO_NS + "SIO_000028");
	}

	public Property getSIOisPartOf() {
		return ontologyModel.getProperty(SIO_NS + "SIO_000068");
	}

	public Property getSIOtransformedFrom() {
		return ontologyModel.getProperty(SIO_NS + "SIO_000657");
	}

	public Property getSIOderivedFrom() {
		return ontologyModel.getProperty(SIO_NS + "SIO_000244");
	}

	public Property getSIOhasDataitem() {
		return ontologyModel.getProperty(SIO_NS + "SIO_001277");
	}

	public Property getSIOisDataitemIn() {
		return ontologyModel.getProperty(SIO_NS + "SIO_001278");
	}

	public Property getSIOHasValue() {
		return ontologyModel.getProperty(SIO_NS + "SIO_000300");
	}

	public Property getSIOisIdentifierFor() {
		return ontologyModel.getProperty(SIO_NS + "SIO_000672");
	}

	public Property getSIOhasIdentifier() {
		return ontologyModel.getProperty(SIO_NS + "SIO_000671");
	}

	public Property getSIOhasAttribute() {
		return ontologyModel.getProperty(SIO_NS + "SIO_000008");
	}

	public Property getSIOisAttributeOf() {
		return ontologyModel.getProperty(SIO_NS + "SIO_000011");
	}

	protected Property getDicomDataProperty(String prop) {
		return ontologyModel.getProperty(DICOM_NS + prop);
	}

	protected Resource getDicomClass(String prop) {
		return ontologyModel.getOntClass(DICOM_NS + prop);
	}

	public Property getPROVWasGeneratedBy() {
		return ontologyModel.getProperty(PROV_NS + "wasGeneratedBy");
	}

	public Property getPROVAssociatedWith() {
		return ontologyModel.getProperty(PROV_NS + "wasAssociatedWith");
	}

	public Property getPROVAttributedTo() {
		return ontologyModel.getProperty(PROV_NS + "wasAttributedTo");
	}

	public String getNIDMStringIRI(String localName) {
		return NIDM_NS + localName;
	}

	public Project createProject(String id) {
		return new Project(id, this);
	}

	public Project createProject(String id, Model model) {
		return new Project(id, this, model);
	}

	public Subject createSubject(String id) {
		return new Subject(id, this);
	}

	public Subject createSubject(String id, Model model) {
		return new Subject(id, this, model);
	}

	public void serialize(String destination) throws IOException {
		this.defaultModel.write(new FileWriter(destination), "TTL");
	}

	public OntModel getOntologyModel() {
		return ontologyModel;
	}
}
