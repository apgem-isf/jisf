package org.isf.formal;

import java.util.ArrayList;
import java.util.List;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.Resource;

public class Project extends AbstractEntity {

	private List<Subject> subjects;
	private List<DataCollection> experiments;

	public Project(String id, FormalLevel fm) {
		super(fm);
		init(id);
	}

	public Project(String id, FormalLevel fm, Model model) {
		super(fm, model);
		init(id);
	}

	private void init(String id) {
		this.entityType = "project";
		this.thisResource = this.model.createResource(getURIFromID(id),
				fm.getSIOClass("SIO_000747"));
		this.subjects = new ArrayList<Subject>();
		this.experiments = new ArrayList<DataCollection>();
		this.setID(id);
	}

	public Subject createSubject(String id) {
		Subject subject = new Subject(id, this.fm, this.model);
		subject.participatesIn(this);
		hasParticipant(subject);
		this.subjects.add(subject);
		return subject;
	}

	public Subject createSubject(String id, Model model) {
		Subject subject = new Subject(id, this.fm, model);
		subject.participatesIn(this);
		hasParticipant(subject);
		this.subjects.add(subject);
		return subject;
	}

	public void addExperiment(DataCollection exp) {
		this.experiments.add(exp);
		hasPart(exp);
	}

	public void hasPart(AbstractEntity resource) {
		this.thisResource.addProperty(fm.getSIOhasPart(),
				resource.getResource());
	}

	public void hasPart(Resource resource) {
		this.thisResource.addProperty(fm.getSIOhasPart(), resource);
	}

	public void hasPart(String resource) {
		this.thisResource.addProperty(fm.getSIOhasPart(), resource);
	}

	public void hasParticipant(AbstractEntity resource) {
		this.thisResource.addProperty(fm.getSIOhasParticipant(),
				resource.getResource());
	}

	public void hasParticipant(Resource resource) {
		this.thisResource.addProperty(fm.getSIOhasParticipant(), resource);
	}

	public void hasParticipant(String resource) {
		this.thisResource.addProperty(fm.getSIOhasParticipant(), resource);
	}

	public List<Subject> getSubjects() {
		return subjects;
	}

	public List<DataCollection> getExperiments() {
		return experiments;
	}

}
