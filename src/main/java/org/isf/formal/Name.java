package org.isf.formal;

import org.apache.jena.rdf.model.Model;

public class Name extends AbstractEntity {
	private static final String NAME_SIO = "SIO_000116";

	public Name(String value, FormalLevel fm, AbstractEntity entity) {
		super(fm);
		init(value, entity);
	}

	public Name(String value, FormalLevel fm, Model model, AbstractEntity entity) {
		super(fm, model);
		init(value, entity);
	}

	private void init(String value, AbstractEntity entity) {
		this.entityType = "ID";
		this.thisResource = this.model.createResource(getURIUID(),
				fm.getSIOClass(NAME_SIO));

		// 'is attribute of' the entity
		this.thisResource.addProperty(fm.getSIOisAttributeOf(),
				entity.getResource());
		// and 'has value'
		this.thisResource.addLiteral(fm.getSIOHasValue(), value);
	}
}
