package org.isf.formal;

import java.util.ArrayList;
import java.util.List;

import org.apache.jena.rdf.model.Literal;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Resource;

public class Dataset extends AbstractEntity {

	protected static final String datasetSIO = "SIO_000089";
	protected AbstractEntity owner;
	protected List<DataItem> dataItems;

	protected Dataset(FormalLevel fm, AbstractEntity parent) {
		super(fm);
		this.model = ModelFactory.createDefaultModel();
		init(parent);
	}

	protected Dataset(FormalLevel fm, Model model, AbstractEntity parent) {
		super(fm, model);
		init(parent);
	}

	protected void init(AbstractEntity parent) {
		this.entityType = "dataset";
		this.dataItems = new ArrayList<DataItem>();
		this.thisResource = this.model.createResource(getURIUID(),
				fm.getSIOClass(datasetSIO));
		this.owner = parent;
		this.relate(parent);
	}

	protected void relate(AbstractEntity parent) {
		if (parent instanceof DataCollection) {
			// The dataset 'is output of' its parent data collection
			this.thisResource.addProperty(fm.getSIOisOutputOf(),
					parent.getResource());
			parent.getResource().addProperty(fm.getSIOhasOutput(),
					this.thisResource);
		} else if (parent instanceof Dataset) {
			// 'is part of' the parent dataset
			this.thisResource.addProperty(fm.getSIOisPartOf(),
					parent.getResource());
			parent.getResource().addProperty(fm.getSIOhasPart(),
					this.thisResource);
		} else {
			throw new IllegalArgumentException(
					"Only a Data collection or Dataset can be specified as owner");
		}
	}

	public void setTransformedFrom(String source) {
		this.thisResource.addProperty(fm.getSIOtransformedFrom(), source);
	}

	public DataItem createDataItem(String name) {
		DataItem item = new DataItem(this.fm, this.model, this);
		item.setName(name);
		this.dataItems.add(item);
		// The dataset 'has data item' the data item
		this.thisResource.addProperty(fm.getSIOhasDataitem(),
				item.getResource());
		return item;
	}

	public void addDicomTagValue(String dicomPropertyName, Object value) {
		Literal literal = this.model.createTypedLiteral(value);
		this.thisResource.addLiteral(
				this.fm.getDicomDataProperty(dicomPropertyName), literal);
	}

	public Dataset createDataset() {
		Dataset set = new Dataset(fm, this.model, this);
		// 'has part' the dataset
		this.thisResource.addProperty(fm.getSIOhasPart(), set.getResource());
		return set;
	}

	public void isAbout(AbstractEntity entity) {
		this.thisResource.addProperty(fm.getSIOisAbout(), entity.getResource());
	}

	public void isAbout(Resource entity) {
		this.thisResource.addProperty(fm.getSIOisAbout(), entity);
	}

	public void isAbout(String entity) {
		this.thisResource.addProperty(fm.getSIOisAbout(), entity);
	}

	public AbstractEntity getOwner() {
		return owner;
	}

	public void setOwner(AbstractEntity owner) {
		this.owner = owner;
	}

	public List<DataItem> getDataItems() {
		return dataItems;
	}
}
