package org.isf.formal;

import org.apache.jena.rdf.model.Literal;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.Resource;

public class DataItem extends AbstractEntity {

	private static final String dataItemSIO = "SIO_000069";

	protected DataItem(FormalLevel fm, Dataset parentDataset) {
		super(fm);
		init(parentDataset);

	}

	protected DataItem(FormalLevel fm, Model model, Dataset parentDataset) {
		super(fm, model);
		init(parentDataset);
	}

	private void init(Dataset parentDataset) {
		this.entityType = "dataitem";
		this.thisResource = this.model.createResource(getURIUID(),
				fm.getSIOClass(dataItemSIO));

		// The data item 'is data item in' its parent Dataset
		this.thisResource.addProperty(fm.getSIOisDataitemIn(),
				parentDataset.getResource());
	}

	/**
	 * the data item 'is derived from' the element defined in the source XSD
	 * model
	 * 
	 * @param source
	 */
	public void setDerivedFrom(String source) {
		this.thisResource.addProperty(fm.getSIOderivedFrom(), source);
	}

	public void setValue(Object value) {
		Literal literal = this.model.createTypedLiteral(value);
		this.thisResource.addLiteral(this.fm.getSIOHasValue(), literal);
	}

	public void setValue(String value, String datatype) {
		Literal literal = this.model.createTypedLiteral(value, datatype);
		this.thisResource.addLiteral(this.fm.getSIOHasValue(), literal);
	}

	public void setName(String name) {
		Name nameNode = new Name(name, fm, model, this);

		// 'has attribute' the name
		this.thisResource.addProperty(fm.getSIOhasAttribute(),
				nameNode.getResource());
	}

	public void addDicomClass(String dcm) {
		Resource c = this.fm.getDicomClass(dcm);
		if (c == null) {
			System.out.println(dcm);
		}
		addClass(c);
	}
}
