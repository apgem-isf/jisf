package org.isf.formal;

import java.util.ArrayList;
import java.util.List;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.Resource;

public class Subject extends AbstractEntity {

	private Project owner;
	List<DataCollection> experiments;

	public Subject(String id, FormalLevel fm) {
		super(fm);
		init(id);

	}

	public Subject(String id, FormalLevel fm, Model model) {
		super(fm, model);
		init(id);

	}

	protected void init(String id) {
		this.entityType = "subject";
		this.experiments = new ArrayList<DataCollection>();
		this.thisResource = this.model.createResource(getURIFromID(id),
				fm.getSIOClass("SIO_000399"));
		this.setID(id);
	}

	/**
	 * Adds the statement <code>this sio:participatesIn resource</code>
	 * 
	 * @param resource
	 *            the resource URI as string
	 */
	public void participatesIn(String resource) {
		this.thisResource.addProperty(fm.getSIOparticipatesIn(), resource);
	}

	/**
	 * Adds the statement <code>this sio:participatesIn resource</code>
	 * 
	 * @param resource
	 *            the {@link Resource} object
	 */
	public void participatesIn(Resource resource) {
		this.thisResource.addProperty(fm.getSIOparticipatesIn(), resource);
	}

	/**
	 * Adds the statement <code>this sio:participatesIn resource</code>
	 * 
	 * @param resource
	 *            the resource wrapped in an {@link AbstractEntity}
	 */
	public void participatesIn(AbstractEntity resource) {
		this.participatesIn(resource.getResource());
	}

	/**
	 * Adds the statement <code>this sio:isSubjectOf resource</code>
	 * 
	 * @param resource
	 *            the resource URI as string
	 */
	public void isSubjectOf(String resource) {
		this.thisResource.addProperty(fm.getSIOisSubjectOf(), resource);
	}

	/**
	 * Adds the statement <code>this sio:isSubjectOf resource</code>
	 * 
	 * @param resource
	 *            the {@link Resource} object
	 */
	public void isSubjectOf(Resource resource) {
		this.thisResource.addProperty(fm.getSIOisSubjectOf(), resource);
	}

	/**
	 * Adds the statement <code>this sio:participatesIn resource</code>
	 * 
	 * @param resource
	 *            the resource wrapped in an {@link AbstractEntity}
	 */
	public void isSubjectOf(AbstractEntity resource) {
		this.isSubjectOf(resource.getResource());
	}

	/**
	 * Creates a new instance of 'Data Collection' in the same model of
	 * <b>this</b> subject and relates it with 'participates in' property.
	 * 
	 * @param id
	 *            the ID for the URI resource
	 * @return the {@link DataCollection} object
	 */
	public Demographics createDemographics(String id) {
		Demographics collection = new Demographics(id, this.fm, this.model,
				this);
		this.participatesIn(collection);
		return collection;
	}

	/**
	 * Creates a new instance of 'Data Collection' in the same model of
	 * <b>this</b> subject and relates it with 'participates in' property.
	 * 
	 * @param id
	 *            the ID for the URI resource
	 * @return the {@link DataCollection} object
	 */
	public DataCollection createDatacollection(String id) {
		DataCollection collection = new DataCollection(id, this.fm, this.model,
				this);
		this.participatesIn(collection);
		this.experiments.add(collection);
		return collection;
	}

	/**
	 * Creates a new instance of 'Data Collection' in the specified model of
	 * relates this subject with it via 'participates in' property.
	 * 
	 * @param id
	 *            the ID for the URI resource
	 * @param model
	 *            the {@link Model} where the statements of this new
	 *            {@link DataCollection} will be stored
	 * @return the {@link DataCollection} object
	 */
	public DataCollection createDatacollection(String id, Model model) {
		DataCollection collection = new DataCollection(id, this.fm, model, this);
		this.participatesIn(collection);
		this.experiments.add(collection);
		return collection;
	}

	/**
	 * Creates a new instance of {@link Session} in the same model of
	 * <b>this</b> subject and relates it with 'participates in' property.
	 * 
	 * @param id
	 *            the ID for the URI resource
	 * @return the {@link Session} object
	 */
	public Session createSession(String id) {
		Session session = new Session(id, this.fm, this.model, this);
		this.participatesIn(session);
		this.experiments.add(session);
		return session;
	}

	/**
	 * Creates a new instance of {@link Session} in the specified model of
	 * relates this subject with it via 'participates in' property.
	 * 
	 * @param id
	 *            the ID for the URI resource
	 * @param model
	 *            the {@link Model} where the statements of this new
	 *            {@link Session} will be stored
	 * @return the {@link Session} object
	 */
	public Session createSession(String id, Model model) {
		Session session = new Session(id, this.fm, model, this);
		this.participatesIn(session);
		this.experiments.add(session);
		return session;
	}

	public void setOwner(Project owner) {
		this.participatesIn(owner);
		this.owner = owner;
	}

	public Project getOwner() {
		return owner;
	}

	public List<DataCollection> getExperiments() {
		return experiments;
	}

}
